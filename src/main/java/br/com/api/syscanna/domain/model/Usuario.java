package br.com.api.syscanna.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import br.com.api.syscanna.domain.model.shared.AbstractEntity;

/**
 * @author marcos olavo
 */

@Entity
@Table(name = "usuarios")
public final class Usuario extends AbstractEntity {

	private String login;
	private String email;
	private String senha;
	private String apelido;

	@NotBlank(message = "Login é obrigatório")
	@Column(length = 20, nullable = false, unique = true)
	@Size(min = 5, max = 20, message = "Login com no mínimo {min} e máximo de {max} caracteres")
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Email(message = "E-mail inválido")
	@Column(nullable = false, unique = true)
	@NotBlank(message = "E-mail é obrigatório")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(length = 60, nullable = false)
	@NotBlank(message = "Senha é obrigatório")
	@Size(min = 6, max = 60, message = "Senha com no mínimo {min} e máximo de {max} caracteres")
	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
}