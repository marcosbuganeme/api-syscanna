package br.com.api.syscanna.domain.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.api.syscanna.domain.model.Mensagem;
import br.com.api.syscanna.domain.model.Usuario;

/**
 * @author marcos olavo
 */
public interface MensagemJpa extends JpaRepository<Mensagem, Integer> {

	List<Mensagem> findTop5ByRemetenteOrderByIdDesc(Usuario remetente);

	List<Mensagem> findTop5ByDestinatarioOrderByIdDesc(Usuario destinatario);
}