package br.com.api.syscanna.domain.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import br.com.api.syscanna.domain.model.shared.AbstractEntity;

/**
 * @author marcos olavo
 */

@Entity
@Table(name = "regras")
public final class Regra extends AbstractEntity {

	private String nome;
	private String descricao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}