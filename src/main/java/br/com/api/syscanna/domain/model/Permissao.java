package br.com.api.syscanna.domain.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import br.com.api.syscanna.domain.model.shared.AbstractEntity;

/**
 * @author marcos olavo
 */

@Entity
@Table(name = "permissoes")
public final class Permissao extends AbstractEntity {

	private Regra regra;
	private Usuario usuario;

	@ManyToOne
	@NotNull(message = "Regra é obrigatória")
	@JoinColumn(name = "id_regra", nullable = false)
	public Regra getRegra() {
		return regra;
	}

	public void setRegra(Regra regra) {
		this.regra = regra;
	}

	@ManyToOne
	@NotNull(message = "Usuário é obrigatório")
	@JoinColumn(name = "id_usuario", nullable = false)
	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}