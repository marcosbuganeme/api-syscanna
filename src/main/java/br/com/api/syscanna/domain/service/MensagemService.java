package br.com.api.syscanna.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.api.syscanna.domain.model.Mensagem;
import br.com.api.syscanna.domain.model.Usuario;
import br.com.api.syscanna.domain.repository.jpa.MensagemJpa;
import br.com.api.syscanna.domain.repository.jpa.UsuarioJpa;
import br.com.api.syscanna.domain.service.exception.ResourceNotFoundException;

/**
 * @author marcos olavo
 */
public @Service class MensagemService {

	private final UsuarioJpa usuarioJpa;
	private final MensagemJpa mensagemJpa;

	public @Autowired MensagemService(UsuarioJpa usuarioJpa, 
						   			  MensagemJpa mensagemJpa) {

		this.usuarioJpa = usuarioJpa;
		this.mensagemJpa = mensagemJpa;
	}

	@Transactional
	public Mensagem save(Mensagem mensagem, Usuario autenticado) {
		validate(mensagem, autenticado);

		return mensagemJpa.save(mensagem);
	}

	@Transactional(readOnly = true)
	public List<Mensagem> lastFiveMessagesReceived(Usuario autenticado) {
		Usuario destinatario = findByUser(autenticado);

		return mensagemJpa.findTop5ByDestinatarioOrderByIdDesc(destinatario);
	}

	@Transactional(readOnly = true)
	public List<Mensagem> lastFiveMessagesSent(Usuario autenticado) {
		Usuario remetente = findByUser(autenticado);

		return mensagemJpa.findTop5ByRemetenteOrderByIdDesc(remetente);
	}

	private void validate(Mensagem mensagem, Usuario autenticado) {
		searchByDestinatario(mensagem);
		searchByRemetente(mensagem, autenticado);
	}

	private void searchByDestinatario(Mensagem mensagem) {
		Usuario destinatario = findByUser(mensagem.getDestinatario());

		if (destinatario == null) {
			throw new ResourceNotFoundException("Destinatário inválido");
		}

		mensagem.setDestinatario(destinatario);
	}

	private void searchByRemetente(Mensagem mensagem, Usuario autenticado) {
		Usuario remetente = findByUser(autenticado);

		if (remetente == null)
			throw new ResourceNotFoundException("Remetente inválido");

		mensagem.setRemetente(remetente);
	}

	private Usuario findByUser(Usuario autenticado) {
		return usuarioJpa.findByLogin(autenticado.getLogin());
	}
}