package br.com.api.syscanna.domain.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.api.syscanna.domain.controller.shared.ControllerShared;
import br.com.api.syscanna.domain.model.Mensagem;
import br.com.api.syscanna.domain.model.Usuario;
import br.com.api.syscanna.domain.service.MensagemService;

/**
 * @author marcos olavo
 */

@RestController
@RequestMapping("v1/protected/mensagens")
public class MensagemController extends ControllerShared {

	private final MensagemService mensagemService;

	public @Autowired MensagemController(MensagemService mensagemService) {
		this.mensagemService = mensagemService;
	}

	@PostMapping
	public ResponseEntity<?> save(@Valid @RequestBody Mensagem mensagem, 
								  @AuthenticationPrincipal UserDetails details) {

		Usuario userAuth = login(details);
		Mensagem saved = mensagemService.save(mensagem, userAuth);

		return ResponseEntity.ok(saved);
	}

	@GetMapping("ultimas/enviadas")
	public ResponseEntity<?> lastReceveid(@AuthenticationPrincipal UserDetails details) {

		Usuario userAuth = login(details);
		List<Mensagem> messages = mensagemService.lastFiveMessagesSent(userAuth);

		if (isValid(messages))
			return ResponseEntity.noContent().build();

		return ResponseEntity.ok(messages);
	}

	@GetMapping("ultimas/recebidas")
	public ResponseEntity<?> lastSent(@AuthenticationPrincipal UserDetails details) {

		Usuario userAuth = login(details);
		List<Mensagem> messages = mensagemService.lastFiveMessagesReceived(userAuth);

		if (isValid(messages))
			return ResponseEntity.noContent().build();

		return ResponseEntity.ok(messages);
	}
}