package br.com.api.syscanna.domain.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.api.syscanna.domain.model.Usuario;

/**
 * @author marcos olavo
 */
public interface UsuarioJpa extends JpaRepository<Usuario, Integer> {

	Usuario findByLogin(String login);
}