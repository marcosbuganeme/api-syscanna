package br.com.api.syscanna.domain.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author marcos olavo
 */

@ResponseStatus(HttpStatus.CONFLICT)
public final class ResourceDuplicateException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public ResourceDuplicateException(String message) {
		super(message);
	}
}
