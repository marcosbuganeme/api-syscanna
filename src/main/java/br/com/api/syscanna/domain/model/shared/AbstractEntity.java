package br.com.api.syscanna.domain.model.shared;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author marcos olavo
 */

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractEntity {

	private Integer id;
	private LocalDateTime criadoAs;
	private LocalDateTime modificadoAs;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@CreatedDate
	@Column(name = "criado_as", nullable = false, updatable = false)
	public LocalDateTime getCriadoAs() {
		return criadoAs;
	}

	public void setCriadoAs(LocalDateTime criadoAs) {
		this.criadoAs = criadoAs;
	}

	@JsonIgnore
	@LastModifiedDate
	@Column(name = "modificado_as", nullable = false, updatable = false)
	public LocalDateTime getModificadoAs() {
		return modificadoAs;
	}

	public void setModificadoAs(LocalDateTime modificadoAs) {
		this.modificadoAs = modificadoAs;
	}
}