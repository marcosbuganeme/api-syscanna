package br.com.api.syscanna.domain.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.api.syscanna.domain.model.shared.AbstractEntity;

/**
 * @author marcos olavo
 */

@Entity
@Table(name = "mensagens")
public final class Mensagem extends AbstractEntity {

	private String assunto;
	private String conteudo;
	private Usuario remetente;
	private Usuario destinatario;

	@Column(nullable = false)
	@NotBlank(message = "Assunto é obrigatório")
	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	@Column(length = 2500, nullable = false)
	@NotBlank(message = "Assunto é obrigatório")
	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	@ManyToOne
	@JoinColumn(name = "id_remetente", nullable = false)
	public Usuario getRemetente() {
		return remetente;
	}

	public void setRemetente(Usuario remetente) {
		this.remetente = remetente;
	}

	@ManyToOne
	@NotNull(message = "Destinatário é obrigatório")
	@JoinColumn(name = "id_destinatario", nullable = false)
	public Usuario getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(Usuario destinatario) {
		this.destinatario = destinatario;
	}
}