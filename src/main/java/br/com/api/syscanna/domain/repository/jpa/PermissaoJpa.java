package br.com.api.syscanna.domain.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.api.syscanna.domain.model.Permissao;

/**
 * @author marcos olavo
 */
public interface PermissaoJpa extends JpaRepository<Permissao, Integer> {

	List<Permissao> findByUsuarioId(Integer idUsuario);
}