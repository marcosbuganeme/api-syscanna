package br.com.api.syscanna.domain.controller.shared;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.api.syscanna.domain.model.Usuario;

/**
 * @author marcos olavo
 */
public abstract class ControllerShared {

	protected Usuario login(UserDetails details) {
		Usuario usuario = new Usuario();
		usuario.setLogin(details.getUsername());
		return usuario;
	}

	protected ResponseEntity<?> noContent() {
		return ResponseEntity.noContent().build();
	}

	protected <T> boolean isNull(T result) {
		return Objects.isNull(result);
	}

	protected boolean isValid(List<?> list) {
		return Optional
				.ofNullable(list)
				.filter(isNull().and(isEmpty()))
				.isPresent();
	}

	private Predicate<List<?>> isNull() {
		return Objects::isNull;
	}

	private Predicate<List<?>> isEmpty() {
		return List::isEmpty;
	}
}