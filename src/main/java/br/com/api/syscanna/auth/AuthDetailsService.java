package br.com.api.syscanna.auth;
import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import br.com.api.syscanna.domain.model.Permissao;
import br.com.api.syscanna.domain.model.Usuario;
import br.com.api.syscanna.domain.repository.jpa.PermissaoJpa;
import br.com.api.syscanna.domain.repository.jpa.UsuarioJpa;

/**
 * @author marcos olavo
 */
public @Component class AuthDetailsService implements UserDetailsService {

	final static String NOT_FOUND = "Usuário [ %s ] não encontrado";

	private final UsuarioJpa usuarioJpa;
	private final PermissaoJpa permissaoJpa;

	public @Autowired AuthDetailsService(UsuarioJpa usuarioJpa, 
										 PermissaoJpa autorizacaoJpa) {

		this.usuarioJpa = usuarioJpa;
		this.permissaoJpa = autorizacaoJpa;
	}

	public @Override UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		Usuario usuario = usuarioJpa.findByLogin(username);

		if (usuario == null)
			new UsernameNotFoundException(format(NOT_FOUND, username));

		return new User(usuario.getLogin(), usuario.getSenha(), findByRoles(usuario));
	}

	private List<GrantedAuthority> findByRoles(Usuario usuario) {
		List<GrantedAuthority> rules = new ArrayList<>();

		permissaoJpa
			.findByUsuarioId(usuario.getId())
			.forEach(add(rules));

		return rules;
	}

	private Consumer<Permissao> add(List<GrantedAuthority> rules) {
		return permissao -> rules.add(new SimpleGrantedAuthority(permissao.getRegra().getNome()));
	}
}