package br.com.api.syscanna;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiSyscannaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiSyscannaApplication.class, args);
	}
}