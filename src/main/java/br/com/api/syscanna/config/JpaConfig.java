package br.com.api.syscanna.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author marcos olavo
 */

@Configuration
@EnableJpaAuditing
@EnableTransactionManagement
@EntityScan(basePackages = { "br.com.api.syscanna.domain.model" })
@EnableJpaRepositories(basePackages = { "br.com.api.syscanna.domain.repository.jpa" })
public class JpaConfig {}