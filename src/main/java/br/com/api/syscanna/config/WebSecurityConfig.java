package br.com.api.syscanna.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.api.syscanna.auth.AuthDetailsService;

/**
 * @author marcos olavo
 */

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	private @Autowired AuthDetailsService authService;

	protected @Override void configure(HttpSecurity http) throws Exception {
		http
		  .headers().frameOptions().disable()
		  	.and()
		  		.authorizeRequests()
			  		.antMatchers("/*/admin/**").hasRole("ADMIN")
				  	.antMatchers("/*/protected/**").hasAnyRole("MANAGER", "ADMIN")
				  	.antMatchers(HttpMethod.GET, "/db/h2-console/**").permitAll()
		  	.and()
		  		.httpBasic()
		  	.and().csrf().disable();
	}

	protected @Override void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(authService).passwordEncoder(new BCryptPasswordEncoder());
	}
}