package br.com.api.syscanna.config;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.context.SecurityContextHolder;

import br.com.api.syscanna.domain.model.Usuario;

/**
 * @author marcos olavo
 */
public class AuditorAwareImpl implements AuditorAware<Usuario> {

	public @Override Optional<Usuario> getCurrentAuditor() {
		return Optional.ofNullable(findByUser());
	}

	private Usuario findByUser() {
		return (Usuario) SecurityContextHolder
								.getContext()
								.getAuthentication()
								.getPrincipal();
	}
}