package br.com.api.syscanna.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author marcos olavo
 */

@EnableAsync
@Configuration
public class AsyncConfig {}