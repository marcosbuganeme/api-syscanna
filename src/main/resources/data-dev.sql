INSERT INTO usuarios(id, criado_as, modificado_as, apelido, login, senha, email) VALUES (1, '2018-08-20T00:56:51.25', '2018-08-20T00:56:51.25', 'ovalo', 'mr.ovalo', '$2a$10$zy86.vwh1kXWhcb2pfODGOHd7Sh8N6Ubz5fU1Uc0NjNzpP3FRnVRK', 'marcos.after@gmail.com');
INSERT INTO usuarios(id, criado_as, modificado_as, apelido, login, senha, email) VALUES (2, '2018-08-20T00:56:51.25', '2018-08-20T00:56:51.25', 'sujej', 'mr.susej', '$2a$10$zy86.vwh1kXWhcb2pfODGOHd7Sh8N6Ubz5fU1Uc0NjNzpP3FRnVRK', 'jefferson.jesus@gmail.com');

INSERT INTO regras(id, criado_as, modificado_as, nome, descricao) VALUES (1, '2018-08-20T00:56:51.25', '2018-08-20T00:56:51.25', 'ROLE_ADMIN', 'visualiza todos os módulos de entrada e inputa todos os dados de cadastros');
INSERT INTO regras(id, criado_as, modificado_as, nome, descricao) VALUES (2, '2018-08-20T00:56:51.25', '2018-08-20T00:56:51.25', 'ROLE_MANAGER', 'visualiza os dados já cadastrados e emite relatórios');

INSERT INTO permissoes(id, criado_as, modificado_as, id_usuario, id_regra) VALUES (1, '2018-08-20T00:56:51.25', '2018-08-20T00:56:51.25', 1, 1);
INSERT INTO permissoes(id, criado_as, modificado_as, id_usuario, id_regra) VALUES (2, '2018-08-20T00:56:51.25', '2018-08-20T00:56:51.25', 1, 2);
INSERT INTO permissoes(id, criado_as, modificado_as, id_usuario, id_regra) VALUES (3, '2018-08-20T00:56:51.25', '2018-08-20T00:56:51.25', 2, 2);

INSERT INTO mensagens (id, criado_as, modificado_as, assunto, conteudo, id_destinatario, id_remetente) VALUES (1, '2018-09-09T09:28:47.365', '2018-09-09T09:28:47.365', 'envio de teste', 'mensagem enviada com a finalidade de testar o funcionamento do recurso da aplicação', 2, 1);