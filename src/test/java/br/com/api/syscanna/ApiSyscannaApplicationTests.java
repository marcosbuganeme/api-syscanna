package br.com.api.syscanna;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@Rollback
@Transactional
@SpringBootTest
@AutoConfigureTestDatabase
@RunWith(SpringRunner.class)
public class ApiSyscannaApplicationTests {

	@Test
	public void initContext() {}
}