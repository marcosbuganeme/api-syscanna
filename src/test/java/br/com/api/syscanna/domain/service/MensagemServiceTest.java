package br.com.api.syscanna.domain.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.api.syscanna.ApiSyscannaApplicationTests;
import br.com.api.syscanna.domain.MockMensagem;
import br.com.api.syscanna.domain.MockUsuario;
import br.com.api.syscanna.domain.model.Mensagem;
import br.com.api.syscanna.domain.model.Usuario;
import br.com.api.syscanna.domain.repository.jpa.UsuarioJpa;

public class MensagemServiceTest extends ApiSyscannaApplicationTests {

	Usuario autenticadoUm;
	Usuario autenticadoDois;

	@Autowired UsuarioJpa usuarioJpa;
	@Autowired MensagemService service;
	@Autowired MockUsuario mockUsuario;
	@Autowired MockMensagem mockMensagem;

	@Before
	public void inicializar() {
		autenticadoUm = createUser();
		autenticadoDois = createUserDestinatario();
	}

	@Test
	public void saveMessage() {
		Mensagem saved = service.save(mockMensagem.sendMessageHamises(), autenticadoUm);

		assertThat(saved).isNotNull();
		assertThat(saved.getAssunto()).isEqualTo("Mensagem de teste");
	}

	@Test
	public void lastFiveMessagesSent() {
		service.save(mockMensagem.sendMessageMoises(), autenticadoUm);
		service.save(mockMensagem.sendMessageMoises(), autenticadoUm);
		service.save(mockMensagem.sendMessageMoises(), autenticadoUm);
		service.save(mockMensagem.sendMessageMoises(), autenticadoUm);
		service.save(mockMensagem.sendMessageMoises(), autenticadoUm);
		service.save(mockMensagem.sendMessageMoises(), autenticadoUm);

		List<Mensagem> messages = service.lastFiveMessagesSent(autenticadoUm);

		assertThat(messages).hasSize(5);
	}

	@Test
	public void lastFiveMessagesReceveid() {
		service.save(mockMensagem.sendMessageHamises(), autenticadoDois);
		service.save(mockMensagem.sendMessageHamises(), autenticadoDois);
		service.save(mockMensagem.sendMessageHamises(), autenticadoDois);
		service.save(mockMensagem.sendMessageHamises(), autenticadoDois);
		service.save(mockMensagem.sendMessageHamises(), autenticadoDois);
		service.save(mockMensagem.sendMessageHamises(), autenticadoDois);

		List<Mensagem> messages = service.lastFiveMessagesReceived(autenticadoDois);

		assertThat(messages).hasSize(5);
	}

	private Usuario createUser() {
		return usuarioJpa.save(mockUsuario.createFirstUser());
	}

	private Usuario createUserDestinatario() {
		return usuarioJpa.save(mockUsuario.createSecondUser());
	}
}