package br.com.api.syscanna.domain;

import org.springframework.stereotype.Component;

import br.com.api.syscanna.domain.model.Mensagem;
import br.com.api.syscanna.domain.model.Usuario;

/**
 * @author marcos olavo
 */
public @Component class MockMensagem {

	public Mensagem sendMessageHamises() {
		Usuario destinatario = new Usuario();
		destinatario.setLogin("mr.moises");

		Mensagem mensagem = new Mensagem();
		mensagem.setDestinatario(destinatario);
		mensagem.setAssunto("Mensagem de teste");
		mensagem.setConteudo("Essa mensagem foi enviada para fins de teste de funcionalidade do sistema");

		return mensagem;
	}
	
	public Mensagem sendMessageMoises() {
		Usuario destinatario = new Usuario();
		destinatario.setLogin("mr.hamises");

		Mensagem mensagem = new Mensagem();
		mensagem.setDestinatario(destinatario);
		mensagem.setAssunto("Mensagem de teste");
		mensagem.setConteudo("Essa mensagem foi enviada para fins de teste de funcionalidade do sistema");

		return mensagem;
	}
}