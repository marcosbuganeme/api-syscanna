package br.com.api.syscanna.domain;

import org.springframework.stereotype.Component;

import br.com.api.syscanna.domain.model.Usuario;

/**
 * @author marcos olavo
 */
public @Component class MockUsuario {

	public Usuario createFirstUser() {
		Usuario hamises = new Usuario();
		hamises.setLogin("mr.hamises");
		hamises.setSenha("hamises123");
		hamises.setEmail("hamises@email.com.br");
		hamises.setApelido("hamises das montanhas");

		return hamises;
	}

	public Usuario createSecondUser() {
		Usuario moises = new Usuario();
		moises.setLogin("mr.moises");
		moises.setSenha("moises123");
		moises.setEmail("moises@email.com.br");
		moises.setApelido("moises da tribo dos braços longos");

		return moises;
	}
}